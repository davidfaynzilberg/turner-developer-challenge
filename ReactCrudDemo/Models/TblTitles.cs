﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models
{
    [BsonIgnoreExtraElements]
    public partial class TblTitle
    {
        [BsonElement("ReleaseYear")]
        public int ReleaseYear { get; set; }

        [BsonElement("Title Name")]
        public string TitleName { get; set; }

        [BsonElement("Title Name Sortable")]
        public string TitleNameSortable { get; set; }

        [BsonElement("Title Id")]
        public string TitleId { get; set; }

        [BsonId]
        public string _Id { get; set; }
    }
}
