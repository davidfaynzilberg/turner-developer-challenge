﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCrudDemo.Models
{
    public class TitleDataAccessLayer
    {
        public async Task<IEnumerable<TblTitle>> GetAllTitlesFilterByTitle(string search)
        {
            IList<TblTitle> returnList = new List<TblTitle>();

            var builder = Builders<BsonDocument>.Filter;
            // This is exact search
            // var filter = builder.Eq("TitleName", search);
            // DF 09/12/2018 Added Regular Expression to match parts of titles
            var filter = Builders<BsonDocument>.Filter.Regex("TitleName", new BsonRegularExpression(".*" + search + ".*"));
            var filteredList = await EATurnerTestDBContext.dbContext.Titles.Find(filter).ToListAsync();

            try
            {
                TblTitle newTblTitle = null;

                    foreach (BsonDocument document in filteredList)
                    {
                            newTblTitle = new TblTitle();
                            newTblTitle.TitleId = document.GetElement("TitleId").Value.ToString();
                            newTblTitle.ReleaseYear = System.Convert.ToInt32(document.GetElement("ReleaseYear").Value);
                            newTblTitle.TitleNameSortable = document.GetElement("TitleNameSortable").Value.ToString();
                            newTblTitle.TitleName = document.GetElement("TitleName").Value.ToString();
                            newTblTitle._Id = document.GetElement("_id").Value.ToString();

                            Console.WriteLine(document);
                            Console.WriteLine();

                            returnList.Add(newTblTitle);
                    }

                return returnList;
            }
            catch
            {
                // DF 09/12/2018 Added for debug only
                // throw new Exception("GetAllTitlesFilterByTitle Exception: ", ex);
                throw;
            }
        }

        public async Task<IEnumerable<TblTitle>> GetAllTitles()
        {
            IList<TblTitle> returnList = new List<TblTitle>();

            try
            {
                TblTitle newTblTitle = null;
                using (IAsyncCursor<BsonDocument> cursor = await EATurnerTestDBContext.dbContext.Titles.FindAsync(new BsonDocument()))
                {
                    while (await cursor.MoveNextAsync())
                    {
                        IEnumerable<BsonDocument> batch = cursor.Current;
                        foreach (BsonDocument document in batch)
                        {
                            newTblTitle = new TblTitle();
                            newTblTitle.TitleId = document.GetElement("TitleId").Value.ToString();
                            newTblTitle.ReleaseYear = System.Convert.ToInt32(document.GetElement("ReleaseYear").Value);
                            newTblTitle.TitleNameSortable = document.GetElement("TitleNameSortable").Value.ToString();
                            newTblTitle.TitleName = document.GetElement("TitleName").Value.ToString();
                            newTblTitle._Id = document.GetElement("_id").Value.ToString();

                            Console.WriteLine(document);
                            Console.WriteLine();

                            returnList.Add(newTblTitle);
                        }
                    }
                }

                return returnList;
            }
            catch
            {
                // DF 09/12/2018 Added for debug only
                // throw new Exception("GetAllTitles Exception: ", ex);
                throw;
            }
        }
    }
}
