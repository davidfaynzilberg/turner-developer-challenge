﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.Linq;

namespace ReactCrudDemo.Models
{
    public class EATurnerTestDBContext
    {
        private IMongoDatabase _database = null;
        private static EATurnerTestDBContext _dbContext = null;

        public EATurnerTestDBContext()
        {
            try
            {
                var connectionString = Startup._config.GetSection("MongoConnection:ConnectionString").Value;
                var dataBaseName = Startup._config.GetSection("MongoConnection:Database").Value;
                var mongoUrl = new MongoUrl(connectionString);

                MongoClientSettings settings = MongoClientSettings.FromUrl(mongoUrl);

                MongoClient mongoClient = new MongoClient(settings);
                if (mongoClient != null)
                    DB = mongoClient.GetDatabase(dataBaseName);

            }
            catch (Exception ex)
            {
                throw new Exception("Can not access to db server.", ex);
            }
        }

        public static EATurnerTestDBContext dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new EATurnerTestDBContext();
                }
                return _dbContext;
            }
        }

        public IMongoDatabase DB
        {
            get
            {
                return _database;
            }

            set
            {
                _database = value;
            }
        }


        public IMongoCollection<BsonDocument> Titles
        {
            get
            {
                return DB.GetCollection<BsonDocument>("Titles");
            }
        }
    }
}
