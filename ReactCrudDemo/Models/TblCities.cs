﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models
{
    public partial class TblCities
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
    }
}
