﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

interface FetchTitleDataState {
    titlesList: TitlesData[];
    loading: boolean;
}

export class FetchTitles extends React.Component<RouteComponentProps<{}>, FetchTitleDataState> {
    constructor() {
        super();
        this.state = { titlesList: [], loading: true };

        fetch('api/Title/Index')
            .then(response => response.json() as Promise<TitlesData[]>)
            .then(data => {
                this.setState({ titlesList: data, loading: false });
            });

       // This binding is necessary to make "this" work in the callback
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);

    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderTitlesTable(this.state.titlesList);

        return <div>
            <h1>Titles Data</h1>
            <p>This component demonstrates fetching Titles data from the server.</p>
            <p>
                <Link to="/addtitle">Create New</Link>
            </p>
            {contents}
        </div>;
    }

    // Handle Delete request for an employee
    private handleDelete(id: number) {
        //if (!confirm("Do you want to delete employee with Id: " + id))
        //    return;
        //else {
        //    fetch('api/Title/Delete/' + id, {
        //        method: 'delete'
        //    }).then(data => {
        //        this.setState(
        //            {
        //                empList: this.state.empList.filter((rec) => {
        //                    return (rec._Id != id);
        //                })
        //            });
        //    });
        //}
    }

    private handleEdit(id: number) {
        this.props.history.push("/employee/edit/" + id);
    }

    // Returns the HTML table to the render() method.
    private renderTitlesTable(empList: TitlesData[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Title Name</th>
                    <th>Release Year</th>
                    <th>Title Name Sortable</th>
                    <th>Title Id</th>
                </tr>
            </thead>
            <tbody>
                {empList.map(emp =>
                    <tr key={emp._Id}>
                        <td></td>
                        <td>{emp._Id}</td>
                        <td>{emp.titleName}</td>
                        <td>{emp.releaseYear}</td>
                        <td>{emp.titleNameSortable}</td>
                        <td>{emp.titleId}</td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
                            //<td>
                        //    <a className="action" onClick={(id) => this.handleEdit(emp._Id)}>Edit</a>  |
                        //    <a className="action" onClick={(id) => this.handleDelete(emp._Id)}>Delete</a>
                        //</td>

}

export class TitlesData {
    _Id: string = "";
    titleName: string = "";
    releaseYear: string = "";
    titleNameSortable: string = "";
    titleId: string = "";
} 