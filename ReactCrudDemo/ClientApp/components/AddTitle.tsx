﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { TitlesData } from './FetchTitles';

interface AddTitleDataState {
    title: string;
    loading: boolean;
    cityList: Array<any>;
    titleData: TitlesData;
}

export class AddTitle extends React.Component<RouteComponentProps<{}>, AddTitleDataState> {
    constructor(props) {
        super(props);

        this.state = { title: "", loading: true, cityList: [], titleData: new TitlesData };

        fetch('api/Title/GetTitleList')
            .then(response => response.json() as Promise<Array<any>>)
            .then(data => {
                this.setState({ cityList: data });
            });

        var titleId = this.props.match.params["titleId"];

        // This will set state for Edit Title
        if (titleId > 0) {
            fetch('api/Title/Details/' + titleId)
                .then(response => response.json() as Promise<TitlesData>)
                .then(data => {
                    this.setState({ title: "Edit", loading: false, titleData: data });
                });
        }

        // This will set state for Add Title
        else {
            this.state = { title: "Create", loading: false, cityList: [], titleData: new TitlesData };
        }

        // This binding is necessary to make "this" work in the callback
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm(this.state.cityList);

        return <div>
            <h1>{this.state.title}</h1>
            <h3>Title</h3>
            <hr />
            {contents}
        </div>;
    }

    // This will handle the submit form event.
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        // PUT request for Edit Title.
        if (this.state.titleData._Id) {
            fetch('api/Title/Edit', {
                method: 'PUT',
                body: data,

            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchtitle");
                })
        }

        // POST request for Add Title.
        else {
            fetch('api/Title/Create', {
                method: 'POST',
                body: data,

            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchtitle");
                })
        }
    }

    // This will handle Cancel button click event.
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/fetchtitle");
    }

    // Returns the HTML Form to the render() method.

                        //<select className="form-control" data-val="true" name="titleId" defaultValue={this.state.titleData.titleId} required>
                        //    <option value="">-- Select Gender --</option>
                        //    <option value="Male">Male</option>
                        //    <option value="Female">Female</option>
    //</select>

    private renderCreateForm(cityList: Array<any>) {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="_Id" value={this.state.titleData._Id} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="Name">Name</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="titleName" defaultValue={this.state.titleData.titleName} required />
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="titleId">Title ID</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="titleId" defaultValue={this.state.titleData.titleId} required />
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="releaseYear">Release Year</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="releaseYear" defaultValue={this.state.titleData.releaseYear} required />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="titleNameSortable">Title Name Sortable</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="titleNameSortable" defaultValue={this.state.titleData.titleNameSortable} required />
                    </div>
                </div >
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}