﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

interface FetchDataState {
    titlesList: TitlesData[];
    loading: boolean;
    searchValue: string;
}

export class FetchTitleByName extends React.Component<RouteComponentProps<{}>, FetchDataState> {
    constructor() {
        super();
        this.state = { titlesList: [], loading: true, searchValue: '' };

        // This binding is necessary to make "this" work in the callback
        this.handleFind = this.handleFind.bind(this);
    }

    public componentDidMount() {
        this.handleFind('')
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderTitlesTable(this.state.titlesList);

        const { searchValue } = this.state;

        return <div className="fetch-title-by-name">
            <h1>Searching Titles Data</h1>
            <p>This component demonstrates search for Title from the Mongo Database. (case sensitive search)</p>
            <p>
                <form className="serach-bar">
                    <input type="text" value={searchValue} onChange={this.handleSearchStringChange} />
                    <button className="btn btn-primary" onClick={this.handleSearch}>Search</button>
               </form>
            </p>
            
            { contents }
        </div>;
    }

    private handleSearchStringChange = (event) => {
        // console.log(event);
        this.setState({ searchValue: event.currentTarget.value })
    }

    private handleSearch = (event) => {
        event.preventDefault();

        this.handleFind(this.state.searchValue)
    }

    // Handle Delete request for an employee
    private handleFind(search: string) {

        this.setState({loading: true})

        fetch(`api/Title/Search?term=${search}`)
            .then(response => response.json() as Promise<TitlesData[]>)
            .then(data => {
                this.setState({ titlesList: data, loading: false });
            });
    }

    // Returns the HTML table to the render() method.
    private renderTitlesTable(titlesList: TitlesData[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Title Name</th>
                    <th>Release Year</th>
                    <th>Title Name Sortable</th>
                    <th>Title Id</th>
                </tr>
            </thead>
            <tbody>
                {titlesList.map(emp =>
                    <tr key={emp._Id}>
                        <td></td>
                        <td>{emp._Id}</td>
                        <td>{emp.titleName}</td>
                        <td>{emp.releaseYear}</td>
                        <td>{emp.titleNameSortable}</td>
                        <td>{emp.titleId}</td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

export class TitlesData {
    _Id: string = "";
    titleName: string = "";
    releaseYear: string = "";
    titleNameSortable: string = "";
    titleId: string = "";
} 