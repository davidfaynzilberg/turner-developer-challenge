import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchTitles } from './components/FetchTitles';
import { FetchTitleByName } from './components/FetchTitleByName';
import { AddTitle } from './components/AddTitle';

export const routes = <Layout>
    <Route exact path='/' component={Home} />
    <Route path='/fetchTitles' component={FetchTitles} />
    <Route path='/fetchTitleByName' component={FetchTitleByName} />
    <Route path='/addtitle' component={AddTitle} />
    <Route path='/title/edit/:titleId' component={AddTitle} />
</Layout>;