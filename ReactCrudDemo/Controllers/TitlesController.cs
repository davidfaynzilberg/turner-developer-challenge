﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReactCrudDemo.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReactCrudDemo.Controllers
{
    public class TitlesController : Controller
    {
        TitleDataAccessLayer objTitle = new TitleDataAccessLayer();

        [HttpGet]
        [Route("api/Title/Index")]
        public async Task<IEnumerable<TblTitle>> Index()
        {
            return await objTitle.GetAllTitles();
        }

        [HttpGet]
        [Route("api/Title/Search")]
        public async Task<IEnumerable<TblTitle>> Search(string term)
        {
            return await objTitle.GetAllTitlesFilterByTitle(term);
        }
    }
}
