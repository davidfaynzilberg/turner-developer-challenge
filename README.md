# CRUD.ASPCore.Reactjs.WebAPI.EF
In this article, we are going to create a web application using ASP.NET Core 2.0 and React.js with the help of Entity Framework Core database first approach. We will be creating a sample Title Record Management system and perform CRUD operations on it. To read the inputs from the user, we are using HTML Form element with required field validations on the client side.

We will be using Visual Studio 2017 and MongoDB

Used
BlueshiftSoftware/EntityFrameworkCore

https://www.myget.org/F/efcore-mongodb/api/v3/index.json

Install-Package -IncludePrerelease Blueshift.EntityFrameworkCore.MongoDb

More Information:
https://github.com/BlueshiftSoftware/EntityFrameworkCore/wiki/Getting-Started